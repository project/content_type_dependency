INTRODUCTION
------------

This module allows admins to create rules. Each of these rules will prevent
a user from adding one particular content type unless the user has finished
adding a specified number of another content type. The number can be
specified by the admin.

The following are some examples of this module features.

* You want a user to add a company page for the user to be able to start adding
  a products page.
* Your user needs to add his profile before he can start posting blogs.


REQUIREMENTS
------------

node module.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules
   for further information.


CONFIGURATION
-------------

* Configure settings at admin/config/content/content_type_dependency/list
